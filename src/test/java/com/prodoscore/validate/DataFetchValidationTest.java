package com.prodoscore.validate;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.prodoscore.core.ReportGenerator;
import com.prodoscore.pojo.FetchError;
import com.prodoscore.util.DataRetriver;
import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;


public class DataFetchValidationTest {

	private DataRetriver retriver;
	private Configuration cfg;

	@BeforeClass
	public void setUp() throws IOException {
		cfg = new Configuration(Configuration.VERSION_2_3_0);
		cfg.setDirectoryForTemplateLoading(new File("templates"));
		cfg.setDefaultEncoding("UTF-8");
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		cfg.setLogTemplateExceptions(false);
		cfg.setWrapUncheckedExceptions(true);

		retriver = new DataRetriver();

	}

	@Test
	public void test_Data_Fetching() {
		try {
			List<FetchError> list = retriver.getErrornousExcecutions();

			if (null != list && !list.isEmpty()) {
				ReportGenerator.printErrorReport(cfg, list);
				Assert.fail("Data fetch errors exists !!! Please see the report for more details ");
			}else {
				ReportGenerator.printSuccessReport(cfg);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(cfg.toString());
			ReportGenerator.printWarningReport(cfg, e);
			e.printStackTrace();
			Assert.fail("Monitoring job exception >> Not a system issue");
		} 
	}

}
