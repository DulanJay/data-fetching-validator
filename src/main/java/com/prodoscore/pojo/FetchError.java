package com.prodoscore.pojo;

public class FetchError {
	
	private String date;
	private String domain;
	private String product;
	private String errordesc;
	private int count;
	
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getErrordesc() {
		return errordesc;
	}
	public void setErrorDesc(String errorDesc) {
		this.errordesc = errorDesc;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	

}
