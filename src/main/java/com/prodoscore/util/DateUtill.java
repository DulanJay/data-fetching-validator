package com.prodoscore.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class DateUtill {

	public static String getCurrentDate() {

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(cal.getTime());
	}

	public static String getCurrentDate(TimeZone zone) {

		Calendar cal = Calendar.getInstance(zone);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(cal.getTime());
	}

	 
	public static String getCurrentTime() {
		Calendar cal = Calendar.getInstance();
		return cal.getTime().toString();
		
	}
	

}
