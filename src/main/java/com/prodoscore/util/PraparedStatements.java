package com.prodoscore.util;

public class PraparedStatements {
	
	
 //public  final static String ERROR_STATUS_SQL = "SELECT * FROM prodoapp_db.proapp_statistic where date=? AND score < 0;";

 
 public final static String ERROR_STATUS_SQL = "SELECT proapp_statistic.date AS Date,proapp_domain.title AS Domain,proapp_statistic.product As Product,proapp_statistic.score As ErrorType,COUNT(*) AS Count\r\n"
 		+ "FROM prodoapp_db.proapp_statistic,prodoapp_db.proapp_employee,prodoapp_db.proapp_domain \r\n"
 		+ "where proapp_statistic.employee_id = proapp_employee.id \r\n"
 		+ "      AND proapp_employee.domain_id=proapp_domain.id \r\n"
 		+ "          AND proapp_statistic.score <0 \r\n"
 		+ "             AND  proapp_statistic.date=?\r\n"
 		+ "GROUP BY\r\n"
 		+ "   proapp_domain.title,proapp_statistic.product,proapp_statistic.score \r\n"
 		+ "HAVING COUNT(*) > 0;";
}
