package com.prodoscore.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.prodoscore.pojo.*;;

public class DataRetriver {

	
	  public List<FetchError> getErrornousExcecutions() throws ClassNotFoundException, SQLException{
		  
		  ArrayList<FetchError> error_list = null;
		  List<String>         ignore_list = null;
		  
		  
		  String env_list = System.getProperty("ignorelist");
		  System.out.println("Ignore List >>>" +env_list);
		  String ignore_string;
		  
		  if(null != env_list && !env_list.isEmpty())
		     ignore_string = env_list;
		  else 
			 ignore_string = Config.getConfig().getProperty("IGNORE.LIST");
		  
		  if(null != ignore_string )
          ignore_list = Arrays.asList(ignore_string.split(","));
          
		  Connection conn = DBSource.getMySQLDataSource().getConnection();
		  PreparedStatement preparedStatement = conn.prepareStatement(PraparedStatements.ERROR_STATUS_SQL);

		  String excecution_date  = System.getProperty("excecdate");
		  System.out.println("excecution_date >>>" +excecution_date);
		  
		  if(null != excecution_date && !excecution_date.isEmpty() && !excecution_date.equalsIgnoreCase("CURRENT_DATE"))
		   preparedStatement.setString(1, excecution_date);
		  else
		   preparedStatement.setString(1, DateUtill.getCurrentDate());
		  
		  ResultSet result = preparedStatement.executeQuery();
           
          rsloop: while (result.next()) {
                
				String Domain = result.getString("Domain");
				String Product = result.getString("Product");
                
				if (null != ignore_list) {

					if (ignore_list.contains(Domain + "_" + Product)) {
						continue rsloop;
					}

				}

				FetchError error = new FetchError();
				error.setDate(result.getString("Date"));
				error.setDomain(Domain);
				error.setProduct(Product);
				error.setErrorDesc(Status.getError(result.getInt("ErrorType")));
				error.setCount(result.getInt("Count"));

				if (null == error_list) {
					error_list = new ArrayList<FetchError>();

				}
				error_list.add(error);
				System.out.println(
						error.getDate() + ">>" + error.getDomain()+ ">>" + error.getProduct()
								+ ">>" + error.getErrordesc() + ">>" + error.getCount());

			}

			result.close();
			preparedStatement.close();
	        
			return error_list;
	  }
	 


}
