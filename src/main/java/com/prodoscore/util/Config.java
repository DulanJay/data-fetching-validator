package com.prodoscore.util;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;


public class Config {
	
	private static Config instance = new Config();
	private Properties prop;
	
	private Config() {
		
		prop = new Properties();
		try {
			prop.load(new FileInputStream(new File("resources/config.properties")));
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}
    
	public static Config getConfig() {
		
		return instance;
	}
	
	public String getProperty(String Key) {
		
		return prop.getProperty(Key);
	}
	
	
}
