package com.prodoscore.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Status {
	
	  @SuppressWarnings("unused")
	private static final Map<Integer, String> ErrorMap;
	    static {
	        Map<Integer, String> aMap = new HashMap<Integer, String>();
	        aMap.put(-1, "Record was added by Regular Cron. NOT YET PROCESSED(-1)");
	        aMap.put(-2, "Record is Currently Processing(-2)");
	        aMap.put(-3, "Record was added by Regular Cron. NOT YET PROCESSED(-3)");
	        aMap.put(-4, "Record is Currently Processing(-4)");
	        aMap.put(-5, "Record was added by Regular Cron. NOT YET PROCESSED(-5)");
	        aMap.put(-6, "Record is Currently Processing(-6)");
	        aMap.put(-7, "Record was added by Regular Cron. NOT YET PROCESSED(-7)");
	        aMap.put(-8, "Record is Currently Processing(-8)");
	        aMap.put(-10, "Data Loading issue in L1(-10)");
	        
	        ErrorMap = Collections.unmodifiableMap(aMap);
	    }
	    
	    
	    public static String getError(int error) {
	    	String errordesc;
	    	try {
				errordesc = ErrorMap.get(error);
			} catch (Exception e) {
				errordesc ="UNDEFINED>>"+Integer.toString(error);
			}
	    	return errordesc;
	    }

}
