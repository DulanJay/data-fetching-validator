package com.prodoscore.util;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import javax.sql.DataSource;

import com.mysql.cj.jdbc.MysqlDataSource;

public class DBSource {

  
    
    public static DataSource getMySQLDataSource() throws ClassNotFoundException {
    	
    	
		Properties props = new Properties();
		MysqlDataSource mysqlDS = null;
		try {
			
			props.load(new FileInputStream(new File("resources/db.properties")));
			mysqlDS = new MysqlDataSource();
			mysqlDS.setURL(props.getProperty("MYSQL_DB_URL"));
			mysqlDS.setUser(props.getProperty("MYSQL_DB_USERNAME"));
			mysqlDS.setPassword(props.getProperty("MYSQL_DB_PASSWORD"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return mysqlDS;
	}
	


}

