package com.prodoscore.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.prodoscore.pojo.FetchError;
import com.prodoscore.util.DateUtill;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

public class ReportGenerator {

	public static final String SuccessHeader = "No Data Fetching Errors Found";
	public static final String ErrorHeader = "Data Fetching Errors Found";
	public static final String WarningHeader = "Exception in the monitoring program";

	public static final String SuccessText = "No data fetching errors found in the DB by the latest monitoring job excecuted at :";
	public static final String ErrorText = "There have been data fetching errors found in the DB by the latest monitoring job excecuted at:";
	public static final String WarningText = "Please note that there is an monitoring job exception and please find the stacktrace below.";

	public static final String SuccessColorCode = "#009879";
	public static final String ErrorColorCode = "#e11010";
	public static final String WarningColorCode = "#e1b010";

	public static void printSuccessReport(Configuration cfg) throws IOException, TemplateException {

		Map<String, Object> root = new HashMap<>();

		root.put("tabletitle", SuccessText);
		root.put("tableHeader", SuccessHeader);
		root.put("excecutionTime", DateUtill.getCurrentTime());
		root.put("jenkins_job_link", "Https://jenkins.com");
		root.put("isErrosExist", false);
		root.put("appErrosExist", false);
		root.put("colorcode", SuccessColorCode);

		generateReport(root, cfg);

	}

	public static void printErrorReport(Configuration cfg, List<FetchError> errors){

		Map<String, Object> root = new HashMap<>();

		root.put("tabletitle", ErrorText);
		root.put("tableHeader", ErrorHeader);
		root.put("excecutionTime", DateUtill.getCurrentTime());
		root.put("jenkins_job_link", "Https://jenkins.com");
		root.put("isErrosExist", true);
		root.put("appErrosExist", false);
		root.put("FetchErros", errors);
		root.put("colorcode", ErrorColorCode);

		generateReport(root, cfg);

	}

	public static void printWarningReport(Configuration cfg, Exception ex) {

		Map<String, Object> root = new HashMap<>();

		root.put("tabletitle", WarningText);
		root.put("tableHeader", WarningHeader);
		root.put("excecutionTime", DateUtill.getCurrentTime());
		root.put("jenkins_job_link", "Https://jenkins.com");
		root.put("isErrosExist", false);
		root.put("appErrosExist", true);
		root.put("colorcode", WarningColorCode);
		root.put("exception", ex.toString());

		generateReport(root, cfg);

	}

	public static void generateReport(Map<String, Object> root, Configuration cfg){
		try {
			Template temp = cfg.getTemplate("alert-template1.ftlh");
			Writer out = new OutputStreamWriter(new FileOutputStream(new File("reports/output.html")));
			temp.process(root, out);
		} catch (TemplateNotFoundException e) {
		
			e.printStackTrace();
		} catch (MalformedTemplateNameException e) {
			
			e.printStackTrace();
		} catch (ParseException e) {
			
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}

}
